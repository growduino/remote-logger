Instalace repo na Windows

Nainstalovat GIT pomoci:
https://git-scm.com/download/win

Naklonovat si repository pomoci:
- dat kdekoliv na disku na nejakem adresari right-click
- spustit "Git GUI here" a pak "clone existing repository"
- do prvniho pole dat adresu na repository (https://gitlab.com/growduino/remote-logger)
- do druhyho pole vybrat disk a zapsat novy nazev adresare (nesmi to byt zadny existujici) (c:\08 Growduino_SensorBox\)
- kliknout na "Clone"

Nainstalovat VSCode
- stahnout a nainstalovat https://code.visualstudio.com/
- v levym vertikalnim menu dat ctverecky "extensions", najit "platformio" a nainstalovat
- restart VSCode a v levym vertikalnim menu dat vetve "source control" a tam "open existing folder"
- vybrat adresar s repo a dat open a ono se to nacte
