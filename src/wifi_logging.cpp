#include <HTTPClient.h>

#include "main.h"
#include <ArduinoJson.h>



extern BoardConfig board_config;

String logging_url = "";
String logging_protocol = "http://";

void wifi_log() {
    if (WIFI_STATUS && board_config.enable_wifi && board_config.enable_ap == false) {
        if (board_config.logger_url == "") {
            if (board_config.logger_host != "") {
                register_self();
                return;
            }
            return;
        }
        if (logging_url == "") {
            logging_url = logging_protocol + board_config.logger_host + "/loggers/logger/"
                          + board_config.logger_url + "/log/";
        }
        ledcWrite(WIFI_LED_CHANNEL, 128);

        Serial.print("D: logging: ");
        Serial.println(logging_url);
        HTTPClient http;
        extern WiFiClient client;
        http.begin(client, logging_url);
        http.addHeader("Content-Type", "application/json");
        Serial.println("D: constructing body");
        int response_code = http.POST(get_command("sensors", ""));
        Serial.print("D: HTTP response code: ");
        Serial.println(response_code);
        http.end();
    } else {
        ledcWrite(WIFI_LED_CHANNEL, 0);
    }
}

String data_for_registering() {
    String data = "{\"name\": \"";
    data += board_config.board_name;
    data += "\", \"mac\": \"";
    data += WiFi.macAddress();
    data += "\", \"config\": ";
    data += get_command("config", "");
    data += "}";
    return data;
}

void register_self() {
    if (WIFI_STATUS && board_config.enable_wifi) {
        ledcWrite(WIFI_LED_CHANNEL, 128);

        HTTPClient http;
        WiFiClient client;
        http.begin(client, "http://" + board_config.logger_host + "/loggers/logger/");
        http.addHeader("Content-Type", "application/json");
        String data = data_for_registering();
        Serial.print("D: data: ");
        Serial.println(data);
        int response_code = http.POST(data);
        Serial.print("D: HTTP response code: ");
        Serial.println(response_code);
        String response = http.getString();
        Serial.print("D: response: ");
        Serial.println(response);

        if (response_code == 201 || response_code == 200) {
            JsonDocument doc;
            deserializeJson(doc, response);
            int id = doc[String("id")];
            Serial.print("D: id: ");
            Serial.println(id);
            board_config.logger_url = String(id);
            Serial.print("D: board config: ");
            Serial.println();
            save_new_config(board_config);
            http.end();
            Serial.println("D: registered, uploading updated config");
            http.begin(client, "http://" + board_config.logger_host
                                   + "/loggers/logger/" + String(id) + "/");
            http.addHeader("Content-Type", "application/json");
            String new_data = data_for_registering();
            Serial.println(new_data);
            int response_code = http.PATCH(new_data);
            Serial.print("D: HTTP response code: ");
            Serial.println(response_code);
        } else {
            ledcWrite(WIFI_LED_CHANNEL, 0);
        }
    }
}