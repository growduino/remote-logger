#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <BH1750.h>
#include <MHZ19.h>
#include <Wire.h>

#include "TCA9548A.h"
#include "main.h"
#include <string.h>

MHZ19 co2_sensor(&Serial2);

void i2c_scan(TwoWire wire) {
    Serial.println(" Scanning I2C Addresses");
    uint8_t cnt = 0;
    for (uint8_t i = 0; i < 128; i++) {
        wire.beginTransmission(i);
        uint8_t ec = wire.endTransmission(true);
        if (ec == 0) {
            if (i < 16) Serial.print('0');
            Serial.print(i, HEX);
            cnt++;
        } else Serial.print("..");
        Serial.print(' ');
        if ((i & 0x0f) == 0x0f) Serial.println();
    }
    Serial.print("Scan Completed, ");
    Serial.print(cnt);
    Serial.println(" I2C Devices found.");
}


#define BUFLEN 32
char serial_buffer[BUFLEN + 1];  //+1 for \0
char read_char;


int count_alnum(const char *s) {
    int count = 0;
    while (*s) {
        if (isalnum(*s)) count++;
        s++;
    }
    return count;
}

bool send_serial_ident() {
    Serial2.flush();
    Serial2.print("ident ");
    delay(100);
    int buffer_index = 0;
    strcpy(serial_buffer, "                            ");
    serial_buffer[0] = 0;
    if (Serial2.available()) {
        while ((Serial2.available()) && (buffer_index < BUFLEN)) {
            read_char = Serial2.read();
            serial_buffer[buffer_index] = read_char;
            buffer_index++;
            delay(2);
        }
        serial_buffer[buffer_index] = 0;
    }
    Serial.print("D: ident :'");
    Serial.print(serial_buffer);
    Serial.print("':");
    for (size_t i = 0; i < strlen(serial_buffer); i++) {
        Serial.printf("%02X ", serial_buffer[i]);
    }
    Serial.println(";");

    return (count_alnum(serial_buffer) > 0);
}

Adafruit_BME280 th_sensor;
BH1750 light_in;
DallasTemp ds1(DALLAS_1);
DallasTemp ds2(DALLAS_2);

bool noop() {
    return true;
}

bool th_is_mad = false;
bool th_detected = false;

bool th_sensor_init() {
#ifdef DEBUG_TH
    Serial.println("TH sensor init");
#endif
    if (th_sensor.begin(0x76, &Wire1)) {
        th_detected = true;
#ifdef DEBUG_TH
        Serial.println("TH sensor OK");
#endif
        return true;
    } else {
        th_detected = false;        
#ifdef DEBUG_TH
        Serial.println("TH sensor not found");
#endif
        return false;
    }
}

float th_read_temp() {
    float temp = th_sensor.readTemperature();
    if ((temp < -120) || (temp > 120) || isnan(temp) || isinf(temp)) {
        th_is_mad = true;
        return MINVALUE;
    }
    th_is_mad = false;
    return temp;
}

float th_read_humidity() {
    if (!th_detected || th_is_mad) {
        return MINVALUE;
    } else {
        return th_sensor.readHumidity();
    }
}

bool light_in_init() {
    return light_in.begin(BH1750::CONTINUOUS_HIGH_RES_MODE, (byte)35U, &Wire1);
}

float light_in_read() {
    float result = light_in.readLightLevel();
    if (result < 0) {
        return MINVALUE;
    }

    return result;
}

bool dallas_1_init() {
    return ds1.measure(true);
}

bool dallas_2_init() {
    return ds2.measure(true);
}

float dallas_1_read() {
    return ds1.read();
}

float dallas_2_read() {
    return ds2.read();
}

bool common_serial_sensor_started = false;

bool init_serial() {
    if (!common_serial_sensor_started) {
        Serial2.begin(19200, SERIAL_8N2, SERIAL_RX, SERIAL_TX);
        common_serial_sensor_started = true;
    }
    return send_serial_ident();
}

bool init_serial_co2() {
    Serial.println("D: init_serial 2 CO2");
    if (common_serial_sensor_started) {
        Serial2.begin(9600, SERIAL_8N1, SERIAL_RX, SERIAL_TX);
        MHZ19_RESULT response = co2_sensor.retrieveData();
        if (response == MHZ19_RESULT_OK) {
            Serial.println("D: Disabling CO2 auto calibration");
            co2_sensor.setAutoCalibration(false);
            common_serial_sensor_started = false;
            return true;
        }
        common_serial_sensor_started = false;
        return false;
    }
    return true;
}

float measure_co2() {
    co2_sensor.retrieveData();
    MHZ19_RESULT response = co2_sensor.retrieveData();
    if (response == MHZ19_RESULT_OK) {
        return (float)co2_sensor.getCO2();
    } else {
        Serial.print("D: CO2 read failed: ");
        Serial.println(response);
        return MINVALUE;
    }
}

float measure_serial(int sleep_ater_m) {
    int buffer_index = 0;

    Serial2.flush();

    Serial2.print("m ");
    long int sensor_value = -1;
    long int sensor_value_triple = -1;
    strcpy(serial_buffer, "                            ");

    int delay_time = 10;
    int max_delay = sleep_ater_m / delay_time;
    int delay_ctr = 0;

    while (!Serial2.available() && delay_ctr++ < max_delay) {
        delay(delay_time);
    }

    if (Serial2.available()) {
        while ((Serial2.available()) && (buffer_index < BUFLEN)) {
            read_char = Serial2.read();
            if (read_char != '\n') {
                serial_buffer[buffer_index] = read_char;
                buffer_index++;
            }
            delay(3);
        }
        serial_buffer[buffer_index] = 0;
        Serial.print("D: serial_buffer ");
        Serial.println(serial_buffer);
        sscanf(serial_buffer, "%ld,%ld", &sensor_value, &sensor_value_triple);

        if (((sensor_value * 3 % 1000000) == sensor_value_triple) && sensor_value >= 0) {
            return float(sensor_value);
        }
    } else {
        Serial.println("D: serial2 no response after m");
    }
    return MINVALUE;
}

float measure_serial_div_1000() {
    return measure_serial(1200) / 1000.0;
}

float measure_serial() {
    return measure_serial(200);
}

float curr_to_ppfd(float current) {
    float k = 2.558;
    float f = 0.97;
    float ppfd = (current / 96.6057) * k * f;
    return ppfd;
}

float measure_par() {
    float sensor_value = measure_serial(2100);
    if ((!isinf(sensor_value)) && (sensor_value >= 0)) {
        float ret = curr_to_ppfd(float(sensor_value));
        return ret;
    }
    return MINVALUE;
}

void doPhSetup() {
#ifdef DEBUG_SENSORS
    Serial.print("D: pH addr ");
    Serial.println(I2C_MAX11647adr, BIN);
#endif

    Wire1.beginTransmission(I2C_MAX11647adr);
    Wire1.write(0b11010110);
    Wire1.write(0b00000010);
    Wire1.endTransmission();  // send the message
}

float measure_ph() {
    if (!send_serial_ident()) {
#ifdef DEBUG_SENSORS
        Serial.println("D: pH no response");
#endif
        return MINVALUE;
    }
    float sensor_value = measure_serial(2100);
    if ((!isinf(sensor_value)) && (sensor_value > 0) && (sensor_value < 16368)) {
        float ret = -1.5 + float(sensor_value) / 963;
        return ret;
    }
    return MINVALUE;
}

float getPhMeasure() {
    int reading = 0;
    int reading2 = 0;

    Wire1.requestFrom(I2C_MAX11647adr, 2);
    if (2 <= Wire1.available()) {  // if two bytes were received
        reading = Wire1.read();  // receive high byte (overwrites previous reading)
        reading2 = Wire1.read();

#ifdef DEBUG_PH
        Serial.print("D: pH hi ");
        Serial.print(reading, BIN);  // print the reading
        Serial.print(", pH lo ");
        Serial.println(reading2, BIN);  // print the reading
#endif

        if ((reading & 0x02) == 0) {
            reading = reading & 0x03;
        }
        reading = reading << 8;  // shift high byte to be high 8 bits
        reading |= reading2;

#ifdef DEBUG_PH
        Serial.print("D: pH as signed int ");
        Serial.println(reading);  // print the reading
#endif

        if (reading == 511) return MINVALUE;

        float reading_f = -1.69 * (float)reading + 700;

        if ((reading_f < 0) || (reading_f > 1400)) return MINVALUE;

        return reading_f / 100;
    } else {
#ifdef DEBUG_PH
        Serial.println("D: pH  MINVALUE (no response)");
#endif
        return MINVALUE;
    }
}
