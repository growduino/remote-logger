#include "main.h"

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <LiquidCrystal_I2C.h>
#include <WebServer.h>
#include <WiFi.h>
#include <Wire.h>
#include <ESPmDNS.h>

WiFiClient client;

Button *config_reset_button;

bool pause_logging = false;
String burst_sensor = "";
int burst_serial = 0;
bool lead_transmit = false;
bool i2c_reset_during_burst = false;
bool arduino_ota_started = false;
bool wifi_ap_started = false;
bool webserver_started = false;

extern BoardConfig board_config;
extern DNSServer dnsServer;

extern WebServer ap_server;
extern WebServer webserver;

Sensor PAR1 = Sensor("PAR1", &measure_par, &init_serial, 1, PORT_TYPE_SERIAL);
Sensor Light1 = Sensor("Light1", &light_in_read, &light_in_init, 1, PORT_TYPE_I2C, true);
Sensor PAR2 = Sensor("PAR2", &measure_par, &init_serial, 2, PORT_TYPE_SERIAL);
Sensor Light2 = Sensor("Light2", &light_in_read, &light_in_init, 2, PORT_TYPE_I2C, true);
Sensor Temp1 = Sensor("Temp1", &th_read_temp, &th_sensor_init, 3, PORT_TYPE_I2C, true);
Sensor Humidity1 = Sensor("Humidity", &th_read_humidity, &noop, 3, PORT_TYPE_I2C, true);
Sensor DS1 = Sensor("Temp2", &dallas_1_read, &dallas_1_init, 4, PORT_TYPE_1W, true);
Sensor DS2 = Sensor("Temp3", &dallas_2_read, &dallas_2_init, 5, PORT_TYPE_1W, true);
Sensor USND = Sensor("Usnd", &measure_serial, &init_serial, 6, PORT_TYPE_SERIAL, true);
Sensor EC = Sensor("EC", &measure_serial_div_1000, &init_serial, 7, PORT_TYPE_SERIAL);
Sensor pH = Sensor("pH", &measure_ph, &init_serial, 8, PORT_TYPE_SERIAL);
Sensor CO2 = Sensor("CO2", &measure_co2, &init_serial_co2, 9, PORT_TYPE_SERIAL);

// Humidity needs to be right after Temp1, as they are one sensor
Sensor *loggers[SENSORS] = { &PAR1, &Light1, &PAR2, &Light2, &Temp1, &Humidity1,
                             &DS1,  &DS2,    &USND, &EC,     &pH,    &CO2 };

LiquidCrystal_I2C lcd(0x27, 20, 4);

int time_offset = 0;

bool config_reloaded = false;

void handle_web() {
    if (wifi_ap_started) {
        // DNS
        dnsServer.processNextRequest();
        // HTTP
        ap_server.handleClient();
    } else if (webserver_started) {
        webserver.handleClient();
    }
}

void led_init() {
    Serial.println("D: Starting leds");

    ledcSetup(WIFI_LED_CHANNEL, WIFI_LED_FREQUENCY, 8);
    ledcAttachPin(WIFI_LED_PIN, WIFI_LED_CHANNEL);
    ledcWrite(WIFI_LED_CHANNEL, 0);


    ledcSetup(BUSY_LED_CHANNEL, BUSY_LED_FREQUENCY, 8);
    ledcAttachPin(BUSY_LED_PIN, BUSY_LED_CHANNEL);
    ledcWrite(BUSY_LED_CHANNEL, 0);


    ledcSetup(SHUTDOWN_LED_CHANNEL, SHUTDOWN_LED_FREQUENCY, 8);
    ledcAttachPin(SHUTDOWN_LED_PIN, SHUTDOWN_LED_CHANNEL);
    ledcWrite(SHUTDOWN_LED_CHANNEL, 0);
}

void setup() {
    Serial.begin(115200);
    Serial.println("\n");
    config_reset_button = createButton(
        35, SHUTDOWN_LED_PIN, SHUTDOWN_LED_CHANNEL, &reset_button_callback);
    setup_mux();

    if (!load_config()) {
        reset_config();
        save_config();
        config_reloaded = true;
    }

    wire1_init();

    led_init();
    lcd_init();

    lcd_publish(board_config.board_name);

    if (config_reloaded) {
        lcd_publish("WARN: Config reset");
        config_reloaded = false;
    }

    Serial.print("D: Starting wifi SSID ");
    Serial.print(board_config.ssid);
    Serial.print(", pass ");
    Serial.println(board_config.password);

    if (board_config.enable_wifi) {
        if (board_config.enable_ap) {
            wifi_ap_started = start_ap();
        } else {
            start_web();
        }
    } else {
        Serial.println("D: Not enabled");
    }

    Serial.println("D: Starting ArduinoOTA");
    ArduinoOTA
        .onStart([]() {
            String type;
            if (ArduinoOTA.getCommand() == U_FLASH) type = "sketch";
            else  // U_SPIFFS
                type = "filesystem";

            // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS
            // using SPIFFS.end()
            Serial.println("Start updating " + type);
        })
        .onEnd([]() {
            Serial.println("\nEnd");
        })
        .onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("D: progress: %u%%\r", (progress / (total / 100)));
        })
        .onError([](ota_error_t error) {
            Serial.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR)
                Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR)
                Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });
}

void in_loop() {
    handle_web();
    lcd_tick();
    check_button(config_reset_button);
}

void log() {
    unsigned long start = millis();
    unsigned long prev_sensor_time = millis();
    unsigned long sensor_time;

#ifdef DEBUG_SENSORS
    Serial.println("D: First round");
#endif
    for (int i = 0; i < SENSORS; i++) {
#ifdef DEBUG_SENSORS
        Serial.print("D: Log ");
        Serial.println(loggers[i]->get_name());
#endif
        loggers[i]->Log(loggers[i]->measure());
        sensor_time = millis();
        in_loop();
        Serial.print("D: Sensor ");
        Serial.print(loggers[i]->get_name());
        Serial.print(" took ");
        Serial.println(sensor_time - prev_sensor_time);
        prev_sensor_time = millis();
    }

    unsigned long r1 = millis();
    Serial.print("D: First round took ");
    Serial.print(r1 - start);
    Serial.println(" ms");
#ifdef DEBUG_SENSORS
    Serial.println("D: Second round");
#endif
    for (int i = 0; i < SENSORS; i++) {
        if (loggers[i]->needs_triple_read()) {
#ifdef DEBUG_SENSORS
            Serial.print("D: Log ");
            Serial.println(loggers[i]->get_name());
#endif
            loggers[i]->Log(loggers[i]->measure());
            sensor_time = millis();
            in_loop();
            Serial.print("D: Sensor ");
            Serial.print(loggers[i]->get_name());
            Serial.print(" took ");
            Serial.println(sensor_time - prev_sensor_time);
            prev_sensor_time = millis();
        }
    }
    unsigned long r2 = millis();
    Serial.print("D: Second round took ");
    Serial.print(r2 - r1);
    Serial.println(" ms");
#ifdef DEBUG_SENSORS
    Serial.println("D: Third round");
#endif
    for (int i = 0; i < SENSORS; i++) {
        if (loggers[i]->needs_triple_read()) {
#ifdef DEBUG_SENSORS
            Serial.print("D: Log ");
            Serial.println(loggers[i]->get_name());
#endif
            loggers[i]->Log(loggers[i]->measure());
            sensor_time = millis();
            in_loop();
            Serial.print("D: Sensor ");
            Serial.print(loggers[i]->get_name());
            Serial.print(" took ");
            Serial.println(sensor_time - prev_sensor_time);
            prev_sensor_time = millis();
        }
    }
    unsigned long r3 = millis();
    Serial.print("D: Third round took ");
    Serial.print(r3 - r2);
    Serial.println(" ms");
}

String ipaddr;
String oldaddr = "0.0.0.0";

void read_usb_serial() {
    if (Serial.available()) {
        String command_line = Serial.readStringUntil('\n');
        process_command(command_line);
    };
}

void process_command(String command_line) {
    String subcommand;
    String value;

    // chop command from command line
    Serial.print("D: command_line:");
    Serial.println(command_line);
    String command = command_line.substring(0, 3);
    Serial.print("D: command:");
    Serial.println(command);

    // split rest of command line into subcommand and value, if possible
    command_line = command_line.substring(3);
    command_line.trim();

    int startpos = command_line.indexOf(" ");
    if (startpos > 0) {
        subcommand = command_line.substring(0, startpos);
        subcommand.trim();

        value = command_line.substring(startpos);
        value.trim();
    } else {
        subcommand = command_line;
        value = "";
    }

    Serial.print("D: subcommand:");
    Serial.println(subcommand);
    if (value != "") {
        Serial.print("D: value:");
        Serial.println(value);
    }

    if (command.equals("set")) {
        Serial.println(set_command(subcommand, value));
    } else if (command.equals("get")) {
        Serial.println(get_command(subcommand, value));
    } else if (command.equals("rst")) {
        // restart
        ESP.restart();
    } else if (command.equals("cfg")) {
        load_config_from_grdw();
    }
}


bool force_measurement = true;
unsigned long int ip_addr_changed_at = 0;
bool publish_ip = true;

unsigned long int IP_ADDR_DISPLAY_FOR = 5 * 60 * 1000;

void display_ip_if_changed() {
    ipaddr = WiFi.localIP().toString();
    if (ipaddr != oldaddr) {
        ip_addr_changed_at = millis();
        oldaddr = ipaddr;
    }
    if (publish_ip && (millis() - ip_addr_changed_at < IP_ADDR_DISPLAY_FOR)) {
        lcd_publish(ipaddr);
        publish_ip = false;
    }
}

void start_ota_if_possible() {
    if (WIFI_STATUS || wifi_ap_started) {
        if (!arduino_ota_started) {
            ArduinoOTA.setHostname(board_config.board_name.c_str());
            ArduinoOTA.begin();
            /*use mdns for host name resolution*/
            if (!MDNS.begin(board_config.board_name.c_str())) {  //http://sbMAC.local
                Serial.println("D: Error setting up MDNS responder!");
            } else {
                Serial.print("D: MDNS response set to ");
                Serial.println(board_config.board_name.c_str());
            }

            arduino_ota_started = true;
        }
        ArduinoOTA.handle();
    }
}

unsigned long last_measure_time = 1;

void loop() {
    start_ota_if_possible();

    display_ip_if_changed();

    if (WIFI_STATUS) {
        mqtt_reconnect();
        if (!webserver_started) {
            Serial.println("D: Loading config from GRDW");
            reload_config_from_logger_host();
            Serial.println("D: config loaded (unchanged)");
            start_web();
        }
    }
    in_loop();

    read_usb_serial();

    if (pause_logging) {

        if (burst_sensor != "") {
            cmd_print_raw(burst_sensor);
        } else if (burst_serial) {
            select_channel_by_type_and_port(PORT_TYPE_SERIAL, burst_serial);

            if (i2c_reset_during_burst) {
                Serial.println("D: PCA reset");
                digitalWrite(I2CMUX_RESET, LOW);
            }

            if (lead_transmit) {
                Serial.println("D: Lead transmit");
                digitalWrite(SERIAL_TX, HIGH);
                delay(10);
            }

            Serial.println(init_serial());
            Serial.println(measure_serial());

            if (i2c_reset_during_burst) {
                Serial.println("D: PCA reset off");
                digitalWrite(I2CMUX_RESET, HIGH);
            }

            delay(500);
        }
    }

    unsigned long sec = getSeconds();
    if (!pause_logging && ((sec % 60 == 1 && last_measure_time != sec) || force_measurement)) {
        Serial.print("D: measuring, sec: ");
        Serial.println(sec);
        Serial.print("D: Wifi status is ");
        Serial.println(WiFi.status());

        ledcWrite(BUSY_LED_CHANNEL, 64);

        unsigned long start = millis();
        log();
        unsigned long end = millis();
        Serial.print("D: Logging took ");
        Serial.print(end - start);
        Serial.println(" ms");
        emit_lcd();
        publish_ip = true;
        //wifi_log();
        mqtt_log();
        force_measurement = false;

        ledcWrite(BUSY_LED_CHANNEL, 0);
    }
    last_measure_time = sec;
    in_loop();
    delay(50);
}
