#include "main.h"

extern int time_offset;

unsigned long getSeconds() {
    unsigned long uptime = millis() / 1000;
    return uptime - time_offset;
}

int daymin(unsigned long seconds) {
    return (seconds / 60) % 1440;
}

int daymin() {
    return daymin(getSeconds());
}