#include <LiquidCrystal_I2C.h>

#include "main.h"
extern LiquidCrystal_I2C lcd;

char lcd_lines[LCD_BUFFER_LINES][LCD_LINE_LEN + 1];
int lcd_last_printed_line, inserted_lines;
long lastrun;

void _lcd_flush() {
    // cleans buffer
    lcd_last_printed_line = 0;
    // lcd.setCursor(0, 0);
    for (int i = 0; i < LCD_BUFFER_LINES; i++) {
        lcd_lines[i][0] = '\0';
    }
    inserted_lines = 0;
    lastrun = -1;
}

void lcd_init() {
    Wire.begin(LCD_SDA, LCD_SCL);

    lcd.init();
    lcd.backlight();

    _lcd_flush();
    lastrun = -1;
}

void lcd_publish(String msg) {
    char buf[LCD_LINE_LEN + 1];
    memset(buf, 0, sizeof(buf));
    msg.toCharArray(buf, sizeof(buf));
    // buf[20] = '\0';
    lcd_publish(buf);
}

void lcd_publish(const char *msg) {
    // Inserts msg into buffer
    Serial.print(F("D: lcd_publish: "));
    Serial.println(msg);
    if (inserted_lines < LCD_BUFFER_LINES) {
        snprintf(lcd_lines[inserted_lines], LCD_LINE_LEN + 1, "%-20s\n", msg);
        // strlcpy((char *)lcd_lines[inserted_lines], msg, LCD_LINE_LEN + 1);
        inserted_lines += 1;
        lastrun = -1;
        lcd_tick();
    } else {
        lcd_print_immediate("lcd buf overflow");
    }
}

void lcd_print_immediate(const char *msg) {
    // injects msg from flash just after buffer start and displays it
    if (inserted_lines > 1) {
        strlcpy((char *)lcd_lines[0], lcd_lines[inserted_lines - 1], LCD_LINE_LEN + 1);
        inserted_lines = 2;
    } else {
        inserted_lines += 1;
    }
    strlcpy((char *)lcd_lines[inserted_lines - 1], (char *)msg, LCD_LINE_LEN + 1);
    Serial.print(F("D: lcd_publish: "));
    Serial.println(lcd_lines[inserted_lines - 1]);
    lastrun = -1;
    lcd_last_printed_line = 0;

    lcd_tick();
}

void lcd_flush() {
    // lcd.init();
    _lcd_flush();
}

void lcd_tick() {
    long int currrun = millis() / (1000 * LCD_DISPLAY_LINES);
    if (currrun != lastrun) {
        lastrun = currrun;

        int lines_to_print = min(LCD_DISPLAY_LINES, inserted_lines);
        // lcd.clear();

        for (int i = 0; i < LCD_DISPLAY_LINES; i++) {
            lcd.setCursor(0, i);
            if (lcd_last_printed_line < inserted_lines) {
                lcd.print(lcd_lines[lcd_last_printed_line]);
            } else {
                lcd.print("                    ");
            }
            lcd_last_printed_line += 1;
        }
        if (lcd_last_printed_line >= inserted_lines) {
            lcd_last_printed_line = 0;
        }
    }
}
