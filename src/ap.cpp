#include "main.h"

extern BoardConfig board_config;
WebServer ap_server;

extern bool wifi_ap_started;

const byte DNS_PORT = 53;
DNSServer dnsServer;

bool maybe_redirect_ap() {
    if (ap_server.hostHeader() != ap_server.client().localIP().toString()) {  // my ip
        Serial.print("D: Request redirected");
        Serial.print(ap_server.hostHeader());
        Serial.println(" to captive portal");
        ap_server.sendHeader(
            "Location", String("http://") + ap_server.client().localIP().toString(), true);
        ap_server.send(302, "text/plain", "");
        ap_server.client().stop();
        return true;
    }
    return false;
}

void path_root_ap() {
    if (maybe_redirect_ap()) {
        return;
    }
    ap_server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    ap_server.sendHeader("Pragma", "no-cache");
    ap_server.sendHeader("Expires", "-1");

    if (ap_server.method() == HTTP_POST) {
        config_from_form(ap_server);
    }

    String p;
    p += "<html><head></head><body><h1>Hello, grower!</h1>";
    p += config_as_form();
    p += F("</body></html>");

    ap_server.send(200, "text/html", p);
}

void path_not_found_ap() {
    if (maybe_redirect_ap()) {
        return;
    }
    String message = F("File Not Found\n\n");
    message += "URI: " + ap_server.uri() + "\n";
    message += "Method: ";
    message += (ap_server.method() == HTTP_GET) ? "GET\n" : "POST\n";
    message += "Arguments: " + ap_server.args();
    Serial.println(ap_server.args());
    message += "\n";

    for (uint8_t i = 0; i < ap_server.args(); i++) {
        message += String(" ") + ap_server.argName(i) + ": " + ap_server.arg(i) + "\n";
    }
    ap_server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    ap_server.sendHeader("Pragma", "no-cache");
    ap_server.sendHeader("Expires", "-1");
    ap_server.send(404, "text/plain", message);
}

bool start_ap() {
    if (board_config.enable_ap) {
        char ssid[32];
     
        board_config.ssid.toCharArray(ssid, sizeof(ssid));
        IPAddress device(8, 8, 8, 8);
        IPAddress netmask(255, 255, 255, 0);

        WiFi.softAPConfig(device, device, netmask);
        WiFi.softAP(ssid);
        IPAddress IP = WiFi.softAPIP();
        lcd_publish("Starting AP");
        Serial.print("D: AP IP");
        Serial.println(IP);
        ap_server.begin();
        /* Setup the DNS ap_server redirecting all the domains to the apIP */
        dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
        dnsServer.start(DNS_PORT, "*", device);

        start_ap_webserver();
        return true;
    }
    return false;
}

void start_ap_webserver() {
    ap_server.on("/", path_root_ap);
    ap_server.onNotFound(path_not_found_ap);
    wifi_ap_started = true;
    ap_server.begin();  // Web ap_server start
}
