// #include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <Update.h>

#include "main.h"

extern BoardConfig board_config;
WebServer webserver;
extern bool webserver_started;

bool maybe_redirect() {
    if (webserver.hostHeader() != webserver.client().localIP().toString()) {  // my ip
        webserver.send(404, "text/plain", "Not found.");
        webserver.client().stop();
        return true;
    }
    return false;
}

void path_not_found() {
    if (maybe_redirect()) {
        return;
    }
    String message = F("File Not Found\n\n");
    message += "URI: " + webserver.uri() + "\n";
    message += "Method: ";
    message += (webserver.method() == HTTP_GET) ? "GET\n" : "POST\n";
    message += "Arguments: " + webserver.args();
    Serial.println(webserver.args());
    message += "\n";

    for (uint8_t i = 0; i < webserver.args(); i++) {
        message += String(" ") + webserver.argName(i) + ": " + webserver.arg(i) + "\n";
    }
    webserver.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    webserver.sendHeader("Pragma", "no-cache");
    webserver.sendHeader("Expires", "-1");
    webserver.send(404, "text/plain", message);
}

void path_root() {
    if (maybe_redirect()) {
        return;
    }
    webserver.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    webserver.sendHeader("Pragma", "no-cache");
    webserver.sendHeader("Expires", "-1");
    if (webserver.method() == HTTP_POST) {
        Serial.println("D: config from form");
        config_from_form(webserver);
    }

    webserver.send(200, "text/html", get_sensors_json());
}


void path_config() {
    if (maybe_redirect()) {
        return;
    }
    webserver.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    webserver.sendHeader("Pragma", "no-cache");
    webserver.sendHeader("Expires", "-1");

    String p;
    p += "<html><head></head><body><h1>SensorBox config</h1>";
    p += config_as_form();
    p += F("</body></html>");

    webserver.send(200, "text/html", p);
}

wl_status_t wifi_begin() {
    char ssid[32];
    char password[64];
    board_config.ssid.toCharArray(ssid, sizeof(ssid));
    board_config.password.toCharArray(password, sizeof(password));
    if (!board_config.use_dhcp) {
        Serial.println("D: static IP");
        IPAddress local_IP, gateway, subnet, primaryDNS, secondaryDNS;
        local_IP.fromString(board_config.box_ipaddr);
        gateway.fromString(board_config.box_gateway);
        subnet.fromString(board_config.box_network);
        primaryDNS.fromString(board_config.box_dns1);
        secondaryDNS.fromString(board_config.box_dns2);

        if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
            Serial.println("D: Wifi failed to configure");
        }
    } else {
        Serial.println("D: using DHCP");
    }

    return WiFi.begin(ssid, password);
}

void start_webserver() {
    webserver.on("/", path_root);
    webserver.on("/config", path_config);
    webserver.on("/reload_config", load_config_from_grdw);
    /*handling uploading firmware file */
    webserver.on(
        "/update", HTTP_POST,
        []() {
            webserver.sendHeader("Connection", "close");
            webserver.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
            ESP.restart();
        },
        []() {
            HTTPUpload& upload = webserver.upload();
            if (upload.status == UPLOAD_FILE_START) {
                Serial.printf("Update: %s\n", upload.filename.c_str());
                if (!Update.begin(UPDATE_SIZE_UNKNOWN)) {  //start with max available size
                    Update.printError(Serial);
                }
            } else if (upload.status == UPLOAD_FILE_WRITE) {
                /* flashing firmware to ESP*/
                if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
                    Update.printError(Serial);
                }
            } else if (upload.status == UPLOAD_FILE_END) {
                if (Update.end(true)) {  //true to set the size to the current progress
                    Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
                } else {
                    Update.printError(Serial);
                }
            }
        });

    webserver.onNotFound(path_not_found);
    webserver_started = true;
    webserver.begin();
}

bool start_web() {
    if (!WIFI_STATUS) {
        wifi_begin();
    }
    if (WIFI_STATUS && !webserver_started) {
        lcd_publish("Starting webserver");
        webserver.begin();

        start_webserver();
        return true;
    }
    return false;
}