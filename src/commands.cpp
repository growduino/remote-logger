#include <WiFi.h>

#include "main.h"

extern BoardConfig board_config;

extern Sensor* loggers[];

extern bool pause_logging;
extern bool force_measurement;
extern String burst_sensor;
extern int burst_serial;
extern bool lead_transmit;
extern bool i2c_reset_during_burst;

void print_raw(String sensor, float raw_value) {
    Serial.print("{\"sensor\": \"");
    Serial.print(sensor);
    Serial.print("\", \"rawvalue\": \"");
    Serial.print(raw_value);
    Serial.println("\"}");
}

void cmd_print_raw(String value) {
    for (int i = 0; i < SENSORS; i++) {
        String logger_name = loggers[i]->get_name();
        if (value.equalsIgnoreCase(logger_name)) {
            float raw_value = loggers[i]->get_raw();
            print_raw(logger_name, raw_value);
        }
    }
}

String nack() {
    return "{\"message\": \"Not recognized\"}";
}
String ack() {
    return "{\"message\": \"OK\"}";
}

String set_command(String subcommand, String value) {
    // failures must immediately return nack

    Serial.println("D: cmdSet");
    if (subcommand.equalsIgnoreCase("config") && value.equalsIgnoreCase("save")) {
        save_config();

    } else if (subcommand.equalsIgnoreCase("name")) {
        if (value.length() > 9) value = value.substring(0, 9);
        board_config.board_name = value;

    } else if (subcommand.equalsIgnoreCase("ssid")) {
        if (value.length() > 32) value = value.substring(0, 32);
        board_config.ssid = value;

    } else if (subcommand.equalsIgnoreCase("password")) {
        if (value.length() > 64) value = value.substring(0, 64);
        board_config.password = value;

    } else if (subcommand.equalsIgnoreCase("enable_wifi")) {
        if (value.equalsIgnoreCase("true")) {
            board_config.enable_wifi = true;
        } else if (value.equalsIgnoreCase("false")) {
            board_config.enable_wifi = false;
        } else return nack();

    } else if (subcommand.equalsIgnoreCase("use_dhcp")) {
        if (value.equalsIgnoreCase("true")) {
            board_config.use_dhcp = true;
        } else if (value.equalsIgnoreCase("false")) {
            board_config.use_dhcp = false;
        } else return nack();

    } else if (subcommand.equalsIgnoreCase("box_ipaddr")) {
        if (value.length() > 40) value = value.substring(0, 40);
        board_config.box_ipaddr = value;

    } else if (subcommand.equalsIgnoreCase("box_network")) {
        if (value.length() > 40) value = value.substring(0, 40);
        board_config.box_network = value;

    } else if (subcommand.equalsIgnoreCase("box_gateway")) {
        if (value.length() > 40) value = value.substring(0, 40);
        board_config.box_gateway = value;

    } else if (subcommand.equalsIgnoreCase("box_dns1")) {
        if (value.length() > 40) value = value.substring(0, 40);
        board_config.box_dns1 = value;

    } else if (subcommand.equalsIgnoreCase("box_dns2")) {
        if (value.length() > 40) value = value.substring(0, 40);
        board_config.box_dns2 = value;

    } else if (subcommand.equalsIgnoreCase("logger_url")) {
        if (value.length() > 255) value = value.substring(0, 255);
        board_config.logger_url = value;

    } else if (subcommand.equalsIgnoreCase("logger_host")) {
        if (value.length() > 255) value = value.substring(0, 255);
        board_config.logger_host = value;

    } else if (subcommand.equalsIgnoreCase("pause")) {
        pause_logging = !pause_logging;
        if (!pause_logging) {  // force immediate measurement after pause
            Serial.println("D: Ending pause, forcing measurement, resetting burst settings");
            force_measurement = true;
            burst_sensor = "";
            burst_serial = 0;
            i2c_reset_during_burst = false;
            lead_transmit = false;
        }
    } else if (subcommand.equalsIgnoreCase("burst")) {
        if (value != "") {
            pause_logging = true;
        }
        burst_sensor = value;
    } else if (subcommand.equalsIgnoreCase("serial")) {
        if (value != "0") {
            pause_logging = true;
        }
        burst_serial = atoi(value.c_str());
    } else if (subcommand.equalsIgnoreCase("lead_tx")) {
        if (value != "0") {
            lead_transmit = true;
        } else {
            lead_transmit = false;
        }
    } else if (subcommand.equalsIgnoreCase("pca_reset")) {
        if (value != "0") {
            i2c_reset_during_burst = true;
        } else {
            i2c_reset_during_burst = false;
        }
    } else {
        return nack();
    }
    return ack();
}

String get_sensors_json() {
    String json = "[";
    for (int i = 0; i < SENSORS; i++) {
        if (i != 0) {
            json.concat(",");
        }
        json.concat(loggers[i]->printjson());
    }
    json.concat("]");
    return json;
}

String get_config_json() {
    String json = "{\"board_name\": \"" + board_config.board_name + "\",";

    if (board_config.enable_wifi) {
        json += "\"enable_wifi\": true,";
    } else {
        json += "\"enable_wifi\": false,";
    }

    json += "\"ssid\": \"" + board_config.ssid + "\",";
    json += "\"password\": \"" + board_config.password + "\",";

    if (board_config.use_dhcp) {
        json += "\"use_dhcp\": true,";
    } else {
        json += "\"use_dhcp\": true,";
    }

    json += "\"box_ipaddr\": \"" + board_config.box_ipaddr + "\",";
    json += "\"box_network\": \"" + board_config.box_network + "\",";
    json += "\"box_gateway\": \"" + board_config.box_gateway + "\",";
    json += "\"box_dns1\": \"" + board_config.box_dns1 + "\",";
    json += "\"box_dns2\": \"" + board_config.box_dns2 + "\",";
    json += "\"logger_host\": \"" + board_config.logger_host + "\",";
    json += "\"logger_url\": \"" + board_config.logger_url + "\" }";
    return json;
}

String get_command(String subcommand, String value) {
    Serial.print("D: cmdGet: ");
    Serial.println(subcommand);
    String json;
    if (subcommand.equalsIgnoreCase("name")) {
        json = "{\"name\": \"";
        json.concat(board_config.board_name);
        json.concat("\"}");
    } else if (subcommand.equalsIgnoreCase("mac")) {
        json = "{\"MAC\": \"";
        json.concat(WiFi.macAddress());
        json.concat("\"}");
    } else if (subcommand.equalsIgnoreCase("time")) {
        json = "{\"time\": ";
        json.concat(getSeconds());
        json.concat(", \"daymin\": ");
        json.concat(daymin());
        json.concat("\"}");
    } else if (subcommand.equalsIgnoreCase("sensors")) {
        json = get_sensors_json();
    } else if (subcommand.equalsIgnoreCase("config")) {
        json = get_config_json();
    } else if (subcommand.equalsIgnoreCase("raw")) {
        cmd_print_raw(value);

    } else {
        json = nack();
    }
    return json;
}
