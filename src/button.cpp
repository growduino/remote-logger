#include "button.h"
Button *createButton(
    uint8_t pin, uint8_t led_pin, uint8_t led_channel, void (*button_pressed)()) {
    Button *button = new Button();
    button->pin = pin;
    button->led_pin = led_pin;
    button->led_channel = led_channel;
    button->lastPressedAt = 0;
    button->button_pressed = button_pressed;
    init_button(button);
    return button;
}

#define BUTTON_DEBUG 1

void init_button(Button *button) {
    pinMode(button->pin, INPUT);
    pinMode(button->led_pin, OUTPUT);
    ledcSetup(button->led_channel, 5, 8);
    ledcAttachPin(button->pin, button->led_channel);
    ledcWrite(button->led_channel, 0);
}


void checker_function(Button *button) {
    unsigned long timePressed = millis() - button->lastPressedAt;
    Serial.println(timePressed);
    if (timePressed > 10000) {
#ifdef BUTTON_DEBUG
        Serial.println("D: Calling callback function");
#endif
        if (button->button_pressed != NULL) {
            button->button_pressed();
        } else {
#ifdef BUTTON_DEBUG
            Serial.println("D: No callback function");
#endif
        }
    }
}

void check_button(Button *button) {
    if (digitalRead(button->pin) == LOW) {
        ledcWrite(button->led_channel, 255);
        if (button->lastPressedAt == 0) {
#ifdef BUTTON_DEBUG
            Serial.println("D: Button pressed");
#endif
            button->lastPressedAt = millis();
        }
        checker_function(button);

    } else {

        if (button->lastPressedAt != 0) {
#ifdef BUTTON_DEBUG
            Serial.println("D: Button released");
#endif
            checker_function(button);
            ledcWrite(button->led_channel, 0);
            button->lastPressedAt = 0;
        }
    }
}



void reset_button_callback() {
    extern BoardConfig board_config;
    Serial.println("D: Resetting config");
    reset_config();
    save_config();

}