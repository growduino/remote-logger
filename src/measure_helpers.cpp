#include <math.h>

#include "main.h"

float return_middle(float values[]) {
#ifdef DEBUG
    Serial.print("D: Input values: ");
    Serial.print(values[0]);
    Serial.print(", ");
    Serial.print(values[1]);
    Serial.print(", ");
    Serial.println(values[2]);
#endif
    float sorted_values[3] = { values[0], values[1], values[2] };

    float tmpval;
    if (sorted_values[0] > sorted_values[1]) {
        tmpval = sorted_values[1];
        sorted_values[1] = sorted_values[0];
        sorted_values[0] = tmpval;
    }

    if (sorted_values[1] > sorted_values[2]) {
        tmpval = sorted_values[2];
        sorted_values[2] = sorted_values[1];
        sorted_values[1] = tmpval;
    }

    if (sorted_values[0] > sorted_values[1]) {
        tmpval = sorted_values[1];
        sorted_values[1] = sorted_values[0];
        sorted_values[0] = tmpval;
    }

#ifdef DEBUG
    Serial.print("D: Output values: ");
    Serial.print(sorted_values[0]);
    Serial.print(", ");
    Serial.print(sorted_values[1]);
    Serial.print(", ");
    Serial.println(sorted_values[2]);
#endif

    if (not(isinf(sorted_values[0]))) {
        return sorted_values[1];  // middle value if there are not any -inftys
    }
    if (isinf(sorted_values[1])) {
        return sorted_values[2];  // last value if second is also -infty
    } else {
        return (sorted_values[1] + sorted_values[2]) / 2;
    }
}
