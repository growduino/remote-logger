#include "TCA9548A.h"
#include "main.h"

TCA9548A I2CMux;

extern bool i2c_reset_during_burst;

void wire1_init() {
    Serial.println("D: starting 2wire");
    pinMode(SENSOR_SDA, OUTPUT);
    pinMode(SENSOR_SCL, OUTPUT);
    // reset i2cmux
    pinMode(I2CMUX_RESET, OUTPUT);
    digitalWrite(I2CMUX_RESET, LOW);
    delay(50);
    digitalWrite(I2CMUX_RESET, HIGH);
    Wire1.begin(SENSOR_SDA, SENSOR_SCL);
    I2CMux.begin(Wire1);
}

bool mux_initialized = false;

void setup_mux() {
    pinMode(MUX_PIN0, OUTPUT);
    pinMode(MUX_PIN1, OUTPUT);
    pinMode(MUX_PIN2, OUTPUT);
    pinMode(MUX_PIN3, OUTPUT);
    mux_initialized = true;
}

void select_serial_mux_channel(int channel) {
    if (channel & 1) {
        digitalWrite(MUX_PIN0, HIGH);
    } else {
        digitalWrite(MUX_PIN0, LOW);
    }
    if (channel & 2) {
        digitalWrite(MUX_PIN1, HIGH);
    } else {
        digitalWrite(MUX_PIN1, LOW);
    }
    if (channel & 4) {
        digitalWrite(MUX_PIN2, HIGH);
    } else {
        digitalWrite(MUX_PIN2, LOW);
    }
    if (channel & 8) {
        digitalWrite(MUX_PIN3, HIGH);
    } else {
        digitalWrite(MUX_PIN3, LOW);
    }
}

void select_channel_by_type_and_port(int type, int port) {
    // Select output channel on the 16 channel MUX or i2c mux
    int channel = port - 1;  // MUX channel is from 0 to 8, ports from 1 to 9

    // Close all channels
    if (! i2c_reset_during_burst) {
        I2CMux.closeAll();
    }
    select_serial_mux_channel(14);
    Serial.print("D: Selecting port ");
    Serial.print(port);
    Serial.print(" that is channel ");
    Serial.print(channel);
    Serial.print(" of type ");
    Serial.println(type);
    if (type == PORT_TYPE_I2C) {
        I2CMux.openChannel(channel);
    } else if (type == PORT_TYPE_1W) {
        // do nothing
    } else {
        select_serial_mux_channel(channel);
    }
}