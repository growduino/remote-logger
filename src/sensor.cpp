#include "main.h"

/* loggers, objects that store measured data */

#include <math.h>

extern Sensor* loggers[];
extern bool wifi_ap_started;

void emit_lcd() {
    /*
     Display what we measured on the lcd
  */
    lcd_flush();
    int ctr = 0;
    for (int i = 0; i < SENSORS; i++) {
        float value = loggers[i]->get_value();
        if (!isinf(value)) {
            ctr++;
            // Serial.println(i);
            // Serial.println( loggers[i]->get_name());
            lcd_publish(loggers[i]->lcd_line());
        }
    }
    String line = "Reported " + String(ctr) + " sensors";
    lcd_publish(line);
    if (wifi_ap_started) {
        lcd_publish("Wifi AP started");
    }
}

Sensor::Sensor(String logger_name, float (*measure_f)(), bool (*init_f)(),
               int port, int type, bool needs_triple_read) {
    this->_needs_triple_read = needs_triple_read;
    this->logger_name = logger_name;
    measure_func = measure_f;
    init_func = init_f;
    mux_port = port;
    mux_type = type;

    last_log_time = 0;
    value = MINVALUE;
    values[0] = MINVALUE;
    values[1] = MINVALUE;
    values[2] = MINVALUE;
    disabled = false;
}

float Sensor::get_raw() {
    Serial.print("D: Measure raw");
    Serial.println(logger_name);

    select_channel_by_type_and_port(mux_type, mux_port);
    bool go_on = (*init_func)();
    if (!go_on) {
        return MINVALUE;
    }
    delay(10);
    float measured_value = (*measure_func)();

    return measured_value;
}

float Sensor::measure() {
    if (!disabled) {
        if (mux_port >= 0) {
            select_channel_by_type_and_port(mux_type, mux_port);
        }
        bool go_on = (*init_func)();
        if (!go_on) {
            return MINVALUE;
        }

        delay(10);
        values[0] = values[1];
        values[1] = values[2];
        values[2] = (*measure_func)();
        float measured_value;
        if (_needs_triple_read) {
            measured_value = return_middle(values);
        } else {
            measured_value = values[2];
        }

        Serial.print("D: val: ");
        Serial.println(measured_value);

        return measured_value;
    } else return MINVALUE;
}

String Sensor::get_name() {
    return String(logger_name);
}

String Sensor::lcd_line() {
    String line = "";
    line.concat(logger_name);
    line.concat(": ");
    line.concat(value);
    return line;
}

bool Sensor::needs_triple_read() {
    return _needs_triple_read;
}

bool Sensor::Available() {
    /*
     returns true if we have not logged this minute
  */

    if (last_log_time == 0) {
        return true;
    }
    return (daymin() != daymin(last_log_time));
}

void Sensor::Log(float new_value) {
    last_log_time = getSeconds();
    value = new_value;
}

String Sensor::printjson() {
    String json;
    json = "{\"name\":\"";
    json.concat(logger_name);
    json.concat("\",\"value\":");
    if (isnan(value) || isinf(value)) {
        json.concat("NaN");
    } else {
        json.concat(value);
    }
    json.concat(",\"age\":");
    json.concat(getSeconds() - last_log_time);
    json.concat(",\"idx\":");
    json.concat(daymin(last_log_time));

    json.concat("}");
    return json;
}
float Sensor::get_value() {
    if (isnan(value) || isinf(value)) {
        return MINVALUE;
    } else {
        return value;
    };
}
