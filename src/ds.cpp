#include "main.h"

/* Dallas onewire thermometers */

#include <DallasTemperature.h>
#include <OneWire.h>

DallasTemp::DallasTemp(int pin) {
    wire = new OneWire(pin);
    sensor = new DallasTemperature(wire);
}

bool DallasTemp::measure(bool waitForConversion = true) {

    if (wire->reset() == 0) {
        Serial.println("D: OneWire reset failed, no devices found");
        return false;
    }

    Serial.println("D: OneWire reset OK");
    DeviceAddress deviceAddress;
    bool have_device = sensor->getAddress(deviceAddress, 0);
    if (!have_device) {
        Serial.println("D: OneWire no device found");
        return false;
    }
    sensor->begin();
    sensor->setResolution(deviceAddress, 11, false);
    sensor->setWaitForConversion(waitForConversion);
    sensor->requestTemperatures();
    return true;
}

float DallasTemp::read() {
    float celsius = sensor->getTempCByIndex(0);
    Serial.print("D: Dallas temp: ");
    Serial.println(celsius);
    if (celsius < -126) return MINVALUE;
    if (celsius > 84) return MINVALUE;
    return celsius;
}