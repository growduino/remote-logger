#include "main.h"
#include <PubSubClient.h>

extern BoardConfig board_config;
extern WiFiClient client;

String topic_name() {
    return "logger/" + board_config.logger_url;
}

const char *logger_host = board_config.logger_host.c_str();

PubSubClient mqtt_client(client);


void on_message(char *topic, byte *message, unsigned int length) {
    Serial.println("D: Message arrived: ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)message[i]);
    }
    Serial.println();
    // handle message arrived
    // convert byte array to str
    char received_message[length + 1];
    memcpy(received_message, message, length);
    received_message[length] = '\0';

    Serial.print("D: [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.println(received_message);
    process_command(received_message);
}

void mqtt_publish(const char *message, int try_count) {

    String topic = "log/" + board_config.logger_url;
    Serial.print("D: Publishing message to topic: ");
    Serial.println(topic);
    Serial.print("D: Message: ");
    Serial.println(message);

    if (!mqtt_client.connected()) {
        mqtt_reconnect();
    }
    boolean result = mqtt_client.publish(topic.c_str(), message);
    if (!result) {
        Serial.println("D: Failed to publish message. Reconnecting...");
        mqtt_reconnect();
        if (try_count > 0) {
            mqtt_publish(message, try_count - 1);
        } else {
            Serial.println("D: Failed to publish message after 10 tries.");
        }
    } else {
        Serial.print("D: Message published at try: ");
        Serial.println(11 - try_count);
    }
}

void mqtt_publish(const char *message) {
    mqtt_publish(message, 10);
}


void mqtt_reconnect() {
    // Loop until we're reconnected
    while (!mqtt_client.connected()) {
        mqtt_client.setBufferSize(1024);
        const char *logger_host = board_config.logger_host.c_str();
        mqtt_client.setServer(logger_host, 1883);
        mqtt_client.setCallback(on_message);
        mqtt_client.setKeepAlive(60);

        Serial.print("D: Attempting MQTT connection...");
        Serial.print("D: logger: ");
        Serial.println(board_config.board_name);
        // Attempt to connect
        if (mqtt_client.connect(board_config.board_name.c_str())) {
            Serial.println("D: connected");
            Serial.print("D: Subscribing to topic: ");
            Serial.println(topic_name());
            // Once connected, publish an announcement...
            // mqtt_client.publish("outTopic", "hello world");
            // ... and resubscribe
            if (!mqtt_client.subscribe(topic_name().c_str())) {
                Serial.println("D: failed to subscribe ... :(");
            }
        } else {
            Serial.print("D: failed, rc=");
            Serial.print(mqtt_client.state());
        }
    }
    mqtt_client.loop();
}

void mqtt_log() {
    mqtt_publish(get_command("sensors", "").c_str());
}