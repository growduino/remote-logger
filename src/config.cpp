#include <EEPROM.h>
#include <WiFi.h>
#include <HTTPClient.h>

#include <ArduinoJson.h>

#include "main.h"

BoardConfig board_config;

bool maybe_save_config(BoardConfig new_config) {

    Serial.print("D: maybe_save_config ");
    if (board_config == new_config) {
        Serial.println("(unchanged)");
        return false;
    }
    board_config = new_config;
    Serial.println("(saving)");
    save_config();
    return true;
}

void save_new_config(BoardConfig config) {
    EEPROM.begin(512);

    int addr = 0;
    EEPROM.writeInt(addr, config.valuecheck);
    addr += sizeof(int);

    EEPROM.writeString(addr, config.board_name);
    addr += config.board_name.length() + 1;

    EEPROM.writeBool(addr, config.enable_wifi);
    addr += sizeof(bool);

    EEPROM.writeBool(addr, config.enable_ap);
    addr += sizeof(bool);

    EEPROM.writeString(addr, config.ssid);
    addr += config.ssid.length() + 1;

    EEPROM.writeString(addr, config.password);
    addr += config.password.length() + 1;

    EEPROM.writeBool(addr, config.use_dhcp);
    addr += sizeof(bool);

    EEPROM.writeString(addr, config.box_ipaddr);
    addr += config.box_ipaddr.length() + 1;

    EEPROM.writeString(addr, config.box_network);
    addr += config.box_network.length() + 1;

    EEPROM.writeString(addr, config.box_gateway);
    addr += config.box_gateway.length() + 1;

    EEPROM.writeString(addr, config.box_dns1);
    addr += config.box_dns1.length() + 1;

    EEPROM.writeString(addr, config.box_dns2);
    addr += config.box_dns2.length() + 1;

    EEPROM.writeString(addr, config.logger_host);
    addr += config.logger_host.length() + 1;

    EEPROM.writeString(addr, config.logger_url);
    // addr += config.logger_url.length() + 1;

    EEPROM.end();
}

String form_line_text(String description, String name, String value) {
    String html = "<p>";
    html += "<label for=\"" + name + "\">" + description + ":";
    html += "<input type=\"text\" name=\"" + name + "\" value=\"" + value + "\">";
    html += "</p>";
    return html;
}

String form_line_checkbox(String description, String name, bool value) {
    String html = "<p>";
    html += "<input type=\"checkbox\" name=\"" + name + "\" value=\"" + name + "\"";
    if (value) {
        html += "checked";
    }
    html += "> ";
    html += "<label for=\"" + name + "\">" + description;
    html += "</p>";
    return html;
}

String form_subheader(String text) {
    String html = "<h3>" + text + "</h3>";
    return html;
}

String config_as_form() {
    String html = "<form action=\"/\" method=\"POST\">\n";
    html += form_line_text("Board name", "board_name", board_config.board_name);
    html += form_line_checkbox("Enable WIFI", "enable_wifi", board_config.enable_wifi);
    html += form_line_checkbox("Enable AP", "enable_ap", board_config.enable_ap);

    html += form_subheader("For AP or Client mode:");
    html += form_line_text("SSID", "ssid", board_config.ssid);
    html += form_line_text("Password", "password", board_config.password);

    html += form_subheader("Client mode options:");
    html += form_line_checkbox("Use DHCP", "use_dhcp", board_config.use_dhcp);

    html += form_subheader("Static network options (no DHCP):");
    html += form_line_text("IP Addresses", "box_ipaddr", board_config.box_ipaddr);
    html += form_line_text("Network", "box_network", board_config.box_network);
    html += form_line_text("Gateway", "box_gateway", board_config.box_gateway);
    html += form_line_text("DNS", "box_dns1", board_config.box_dns1);

    html += form_subheader("Logging configuration:");
    html += form_line_text("Logging host:", "logger_host", board_config.logger_host);
    html += form_line_text("Logging URL:", "logger_url", board_config.logger_url);

    html += "<p><input type=\"submit\" name=\"Save\" value=\"Save\" /></p>";
    html += "</form>\n";
    return html;
}

void config_from_form(WebServer &server) {
    BoardConfig new_config;
    new_config.valuecheck = SENSOR_CONFIG_VALUECHECK;
    new_config.enable_wifi = false;
    new_config.enable_ap = false;
    new_config.use_dhcp = false;
    for (uint8_t i = 0; i < server.args(); i++) {
        String field = server.argName(i);
        String value = server.arg(i);

        Serial.print("D: config field ");
        Serial.print(field);
        Serial.print(" value ");
        Serial.println(value);

        if (field == "board_name") {
            new_config.board_name = value;
        } else if (field == "enable_wifi") {
            new_config.enable_wifi = true;
        } else if (field == "enable_ap") {
            new_config.enable_ap = true;
        } else if (field == "use_dhcp") {
            new_config.use_dhcp = true;
        } else if (field == "ssid") {
            new_config.ssid = value;
        } else if (field == "password") {
            new_config.password = value;
        } else if (field == "box_ipaddr") {
            new_config.box_ipaddr = value;
        } else if (field == "box_network") {
            new_config.box_network = value;
        } else if (field == "box_gateway") {
            new_config.box_gateway = value;
        } else if (field == "box_dns1") {
            new_config.box_dns1 = value;
        } else if (field == "box_dns2") {
            new_config.box_dns2 = value;
        } else if (field == "logger_host") {
            new_config.logger_host = value;
        } else if (field == "logger_url") {
            new_config.logger_url = value;
        }
    }
    if (maybe_save_config(new_config)) {
        // reboot after saving
        ESP.restart();
    };
}

void reset_config() {
    Serial.println("D: Resetting config");
    String mac = WiFi.macAddress();
    board_config.valuecheck = SENSOR_CONFIG_VALUECHECK;
    board_config.enable_wifi = true;
    board_config.enable_ap = true;
    board_config.board_name = "SB " + mac;
    mac.replace(":", "");
    board_config.ssid = "SB-" + mac;
    board_config.password = "";
    board_config.logger_host = "";
    board_config.logger_url = "";
    board_config.use_dhcp = true;
}

bool load_config() {
    EEPROM.begin(512);
    int addr = 0;
    board_config.valuecheck = EEPROM.readInt(addr);
    if (board_config.valuecheck != SENSOR_CONFIG_VALUECHECK) {
        EEPROM.end();
        return false;
    }
    addr += sizeof(int);

    board_config.board_name = EEPROM.readString(addr);
    addr += board_config.board_name.length() + 1;

    board_config.enable_wifi = EEPROM.readBool(addr);
    addr += sizeof(bool);

    board_config.enable_ap = EEPROM.readBool(addr);
    addr += sizeof(bool);

    board_config.ssid = EEPROM.readString(addr);
    addr += board_config.ssid.length() + 1;

    board_config.password = EEPROM.readString(addr);
    addr += board_config.password.length() + 1;

    board_config.use_dhcp = EEPROM.readBool(addr);
    addr += sizeof(bool);

    board_config.box_ipaddr = EEPROM.readString(addr);
    addr += board_config.box_ipaddr.length() + 1;

    board_config.box_network = EEPROM.readString(addr);
    addr += board_config.box_network.length() + 1;

    board_config.box_gateway = EEPROM.readString(addr);
    addr += board_config.box_gateway.length() + 1;

    board_config.box_dns1 = EEPROM.readString(addr);
    addr += board_config.box_dns1.length() + 1;

    board_config.box_dns2 = EEPROM.readString(addr);
    addr += board_config.box_dns2.length() + 1;

    board_config.logger_host = EEPROM.readString(addr);
    addr += board_config.logger_host.length() + 1;

    board_config.logger_url = EEPROM.readString(addr);
    // addr += board_config.logger_url.length() + 1;

    EEPROM.end();
    return true;
}

void save_config() {
    save_new_config(board_config);
    // reboot after saving
    ESP.restart();
}

void config_from_json(String json) {
    BoardConfig new_config;
    JsonObject doc;
    new_config.valuecheck = SENSOR_CONFIG_VALUECHECK;
    new_config.enable_wifi = false;
    new_config.enable_ap = false;
    new_config.use_dhcp = false;

    DynamicJsonDocument complete_doc(1024);
    DeserializationError error = deserializeJson(complete_doc, json);
    if (error) {
        Serial.print(F("D: deserializeJson() failed: "));
        Serial.println(error.c_str());
        new_config.valuecheck = 0;
        return;
    }

    if (complete_doc.containsKey("config")) {
        doc = complete_doc["config"];
    } else {
        doc = complete_doc.as<JsonObject>();
    }

    new_config.board_name = doc["board_name"].as<String>();
    new_config.enable_wifi = doc["enable_wifi"].as<bool>();
    new_config.enable_ap = doc["enable_ap"].as<bool>();
    new_config.ssid = doc["ssid"].as<String>();
    new_config.password = doc["password"].as<String>();
    new_config.use_dhcp = doc["use_dhcp"].as<bool>();
    new_config.box_ipaddr = doc["box_ipaddr"].as<String>();
    new_config.box_network = doc["box_network"].as<String>();
    new_config.box_gateway = doc["box_gateway"].as<String>();
    new_config.box_dns1 = doc["box_dns1"].as<String>();
    new_config.box_dns2 = doc["box_dns2"].as<String>();
    new_config.logger_host = doc["logger_host"].as<String>();
    new_config.logger_url = doc["logger_url"].as<String>();

    if (maybe_save_config(new_config)) {
        // reboot after saving
        ESP.restart();
    }
}


void load_config_from_grdw() {
    String logging_protocol = "http://";
    String config_url = logging_protocol + board_config.logger_host
                        + "/loggers/logger/" + board_config.logger_url + "/";
    Serial.print("D: loading config from ");
    Serial.println(config_url);
    HTTPClient http;
    WiFiClient client;
    http.begin(client, config_url);
    int response_code = http.GET();
    if (response_code == 200) {
        String payload = http.getString();
        Serial.println("D: reloading config (have payload)");
        Serial.println(payload);
        config_from_json(payload);
    } else if (response_code == 404) {
        if (board_config.logger_url != "") {
            register_self();
        }
    }
    http.end();
}


void reload_config_from_logger_host() {
    if (board_config.logger_url != "") {
        if (board_config.logger_host != "") {
            Serial.println("D: reloading config");
            load_config_from_grdw();
        }
    }
}
