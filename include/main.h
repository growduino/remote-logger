#pragma once

#define BH1750_QUIET true

#include "Arduino.h"
// #include "util.h"
#include <math.h>

#define MINVALUE -INFINITY

#define OUTPUTS 12
#define RELAY_START 36

#define SENSORS 12

#define SENSOR_SDA 21
#define SENSOR_SCL 22
#define I2CMUX_RESET 23
#define LCD_SDA 25
#define LCD_SCL 26

#define SERIAL_RX 16
#define SERIAL_TX 17

// #define USND_RX 52
// #define USND_TX 30

#define DALLAS_1 27
#define DALLAS_2 14

// i2c pH sensor
#define I2C_MAX11647adr 0x36

#define USE_EC_SENSOR 1
#define EC_SAMPLE_TIMES 100
#define EC_CUTOFF 550
#define EC_TIMEOUT 10000

// when reading from ADC, values read higher than this are considered as error
// (CO2 sensor)
#define ADC_CUTOFF 1020
#define CO2_DATA 12

// LCD

#define LCD_BUFFER_LINES 20
#define LCD_DISPLAY_LINES 4
#define LCD_LINE_LEN 20

// WIFI LED
#define WIFI_LED_CHANNEL 1
#define WIFI_LED_FREQUENCY 5000
#define WIFI_LED_PIN 15

// Busy LED
#define BUSY_LED_CHANNEL 3
#define BUSY_LED_FREQUENCY 5000
#define BUSY_LED_PIN 2

// SHUTDOWN LED
#define SHUTDOWN_LED_CHANNEL 2
#define SHUTDOWN_LED_FREQUENCY 5000
#define SHUTDOWN_LED_PIN 12

// #define DEBUG 1
#ifdef DEBUG
#define DEBUG_PH 1
#endif

#define DEBUG_SENSORS true

#define MUX_PIN0 19
#define MUX_PIN1 18
#define MUX_PIN2 5
#define MUX_PIN3 4

#define MUX2_PIN0 27
#define MUX2_PIN1 14

#include "ap.h"
#include "commands.h"
#include "config.h"
#include "ds.h"
#include "lcd.h"
#include "measure_helpers.h"
#include "mux.h"
#include "sensor.h"
#include "sensors.h"
#include "web_server.h"
#include "wifi_logging.h"
#include "button.h"
#include "pubsub.h"

unsigned long getSeconds();


void process_command(String command_line);

int daymin(unsigned long seconds);

int daymin();

#define WIFI_STATUS (WiFi.status() == WL_CONNECTED)

#define PORT_TYPE_I2C 1
#define PORT_TYPE_1W 2
#define PORT_TYPE_SERIAL 0
#define PORT_TYPE_TEST 3
