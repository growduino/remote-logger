#pragma once

// when you change this number, all settings will be deleted after upload and restart
#define SENSOR_CONFIG_VALUECHECK 213

struct BoardConfig {
    int valuecheck;
    String board_name;
    bool enable_wifi = true;
    bool enable_ap = true;
    String ssid;
    String password;
    bool use_dhcp = true;
    String box_ipaddr;
    String box_network;
    String box_gateway;
    String box_dns1;
    String box_dns2;
    String logger_host;
    String logger_url;
    bool operator==(const BoardConfig& other) const {
        if (valuecheck != SENSOR_CONFIG_VALUECHECK) {  // uninitialized or failed to load
            return false;
        }
        return valuecheck == other.valuecheck
            && board_name == other.board_name
            && enable_wifi == other.enable_wifi
            && enable_ap == other.enable_ap
            && ssid == other.ssid
            && password == other.password
            && use_dhcp == other.use_dhcp
            && box_ipaddr == other.box_ipaddr
            && box_network == other.box_network
            && box_gateway == other.box_gateway
            && box_dns1 == other.box_dns1
            && box_dns2 == other.box_dns2
            && logger_host == other.logger_host
            && logger_url == other.logger_url;
    }
};

String config_as_form();
void config_from_form(WebServer &server);

bool load_config();
void save_config();
void save_new_config(BoardConfig);
void reset_config();
void config_from_json(String json);
void load_config_from_grdw();
void reload_config_from_logger_host();