#pragma once
#include "main.h"
// Struct that holds last state and uptime of a button
#include <cstdint>
typedef struct {
    uint8_t pin;
    uint8_t led_pin;
    uint8_t led_channel;    
    unsigned long lastPressedAt;
    void (*button_pressed)();
} Button;


Button * createButton(uint8_t pin, uint8_t led_pin, uint8_t led_channel, void (*button_pressed)());
void init_button(Button *button);
void check_button(Button *button);
void reset_button_callback();