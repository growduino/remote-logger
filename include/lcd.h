#pragma once

void lcd_init();
void lcd_publish(String msg);
void lcd_publish(const char* msg);
void lcd_print_immediate(const char* msg);
void lcd_flush();
void lcd_tick();