#include <Wire.h>

void i2c_scan(TwoWire wire);

bool noop();

bool th_sensor_init();
bool light_in_init();
bool dallas_1_init();
bool dallas_2_init();
bool init_serial();
bool init_serial_co2();

float th_read_temp();
float th_read_humidity();
float light_in_read();
float dallas_1_read();
float dallas_2_read();

float measure_serial();
float measure_serial_div_1000();
float measure_par();
float measure_co2();
float measure_ph();