#pragma once

#include <DallasTemperature.h>
#include <OneWire.h>

class DallasTemp {
  public:
    explicit DallasTemp(int pin);
    bool measure(bool);
    float read();

  private:
    int pin;
    OneWire* wire;
    DallasTemperature* sensor;
};
