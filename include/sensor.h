#pragma once

#include "main.h"

void emit_lcd();

class Sensor {
    String logger_name;
    float value;
    unsigned long last_log_time;
    int mux_port;
    int mux_type;
    bool disabled;
    float values[3];
    bool _needs_triple_read;

  public:
    Sensor(String logger_name, float (*measure_f)(), bool (*init_f)(),
           int port, int type, bool needs_triple_read = true);
    String get_name();
    String printjson();
    String lcd_line();
    float get_raw();
    bool Available();
    void Log(float new_value);
    float measure();
    float (*measure_func)();
    bool (*init_func)();
    float get_value();
    bool needs_triple_read();
};
