#pragma once

#include "main.h"

void on_message(char *topic, byte *message, unsigned int length);
void mqtt_reconnect();
void mqtt_log();
void mqtt_publish(const char *message);